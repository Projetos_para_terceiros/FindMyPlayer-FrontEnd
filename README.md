# Find My Player - Aplicativo

**Progresso:** CANCELADO<br />
**Autor:** Paulo Victor de Oliveira Leal e Vinicius Luis Lopes Nunes<br />
**Data:** 2017<br />

### Objetivo
Implementar o aplicativo para comunicar com o servidor.

### Observação

IDE:  [Visual Studio Code](https://code.visualstudio.com/)<br />
Linguagem: [Angular](https://angular.io/)<br />
Banco de dados: Não utiliza<br />

### Execução

    $ ng serve
    

### Contribuição

Esse projeto não está concluído e livre para consulta.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->