import { BaseModel } from './BaseModel';
import { LeagueOfLegends } from './LeagueOfLegends';
import { TipoAtivacao } from "./TipoAtivacao";
export class Usuario extends BaseModel {
  usuarioId?: number;
  email: string;
  senhaHash?: string;

  leagueOfLegendsId?: number;
  leagueOfLegends?: LeagueOfLegends;

  tipoAtivacaoId?: number;
  tipoAtivacao?: TipoAtivacao;

  emailConfirmation?: string;
  password?: string;
  passwordConfirmation?: string;

}
