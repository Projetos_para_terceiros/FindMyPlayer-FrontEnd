import { BaseModel } from './BaseModel';

export class Counter extends BaseModel{
  counterId: number;
  heroiId: number;
  heroiInimigoId: number;
}