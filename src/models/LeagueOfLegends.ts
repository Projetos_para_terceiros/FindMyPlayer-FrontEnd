import { BaseModel } from './BaseModel';
import { Lane } from './Lane';
import { LeagueOfLegends_Regiao } from "./LeagueOfLegends_Regiao";
import { Heroi } from "./Heroi";

export class LeagueOfLegends extends BaseModel {
    leagueOfLegendsId: number;
    summonerId: number;
    nick: string;
    level: number;
    imagem: string;
    regiaoId: number;
    regiao: LeagueOfLegends_Regiao;
    heroiPrincipalId: number;
    heroiPrincipal: Heroi;
    heroiSecundarioId: number;
    heroiSecundario: Heroi;
    lanePrincipalId: number;
    lanePrincipal: Lane;
    laneSecundariaId: number;
    laneSecundaria: Lane;
    constructor(lol: any = undefined) {
        super();
        if (lol) {
            this.leagueOfLegendsId = lol.leagueOfLegendsId;
            this.summonerId = lol.summonerId;
            this.nick = lol.nick;
            this.level = lol.level;
            this.imagem = lol.imagem;
            this.regiaoId = lol.regiaoId;
            this.regiao = lol.regiao;
            this.heroiPrincipalId = lol.heroiPrincipalId;
            this.heroiPrincipal = lol.heroiPrincipal;
            this.heroiSecundarioId = lol.heroiSecundarioId;
            this.heroiSecundario = lol.heroiSecundario;
            this.lanePrincipalId = lol.lanePrincipalId;
            this.lanePrincipal = lol.lanePrincipal;
            this.laneSecundariaId = lol.laneSecundariaId;
            this.laneSecundaria = lol.laneSecundaria;
        }
    }
    validar() {
        if (this.heroiPrincipal)
            this.heroiPrincipalId = this.heroiPrincipal.heroiId;
        if (this.heroiSecundario)
            this.heroiSecundarioId = this.heroiSecundario.heroiId;
        if (this.regiao)
            this.regiaoId = this.regiao.leagueOfLegends_RegiaoId;
        return this.nick &&
            this.regiaoId &&
            this.heroiPrincipalId &&
            this.heroiSecundarioId &&
            this.lanePrincipalId &&
            this.laneSecundariaId;
    }
}