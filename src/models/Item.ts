import { BaseModel } from './BaseModel';
import { Jogo } from './Jogo';
export class Item extends BaseModel{
    itemId: number;
    nome?: string;
    descricao?: string;
    valor?: number;
    imagem?: string;
    jogoId?: number;
    jogo?: any;
    versao?: string;
    status?: string;
}