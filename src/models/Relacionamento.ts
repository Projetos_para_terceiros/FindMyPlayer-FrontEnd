import { BaseModel } from './BaseModel';
import { TipoRelacionamento } from './TipoRelacionamento';
import { Usuario } from "./Usuario";

export class Relacionamento extends BaseModel{
     relacionamentoId?: number;
     usuario1Id?: number;
     usuario1?: Usuario;
     tipoRelacionamentoId?: number;
     tipoRelacionamento?: TipoRelacionamento;
     usuario2Id?: number;
     usuario2?: Usuario;
}