import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LolAccountModalPage } from './lol-account-modal';

@NgModule({
  declarations: [
    LolAccountModalPage,
  ],
  imports: [
    IonicPageModule.forChild(LolAccountModalPage),
  ],
})
export class LolAccountModalPageModule {}
