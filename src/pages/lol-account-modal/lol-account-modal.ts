import { LeagueoflegendsRegioesServiceProvider } from './../../providers/leagueoflegends-regioes-service/leagueoflegends-regioes-service';
import { LeagueOfLegends_Regiao } from './../../models/LeagueOfLegends_Regiao';
import { LeagueoflegendsServiceProvider } from './../../providers/leagueoflegends-service/leagueoflegends-service';
import { Heroi } from './../../models/Heroi';
import { LeagueOfLegends } from './../../models/LeagueOfLegends';
import { SelecionarHeroiModalPage } from './../selecionar-heroi-modal/selecionar-heroi-modal';
import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController, ModalController, LoadingController, AlertController } from 'ionic-angular';

import { UsuarioServiceProvider } from './../../providers/usuario-service/usuario-service';
import { HeroisServiceProvider } from './../../providers/herois-service/herois-service';
import { Usuario } from "../../models/Usuario";
/**
 * Generated class for the LolAccountModalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lol-account-modal',
  templateUrl: 'lol-account-modal.html',
  providers: [HeroisServiceProvider, UsuarioServiceProvider]
})
export class LolAccountModalPage {
  lolAccount: LeagueOfLegends;
  regioes: LeagueOfLegends_Regiao[];
  usuario: Usuario;
  //  = {
  //   Imagem: 'https://avatarfiles.alphacoders.com/756/75641.jpg',
  //   Nick: 'fulanex'
  // };

  constructor(public viewCtrl: ViewController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private apiHerois: HeroisServiceProvider,
    private apiUsuario: UsuarioServiceProvider,
    private apiLol: LeagueoflegendsServiceProvider,
    private apiLolRegiao: LeagueoflegendsRegioesServiceProvider) {
    this.lolAccount = new LeagueOfLegends();
    this.usuario = navParams.data.usuarioEscolhido as Usuario;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LolAccountModalPage');
    this.apiLolRegiao.getLeagueOfLegends_Regioes().subscribe(
      data => this.setarRegioes(data),
      erro => alert(erro));
  }

  setarRegioes(data) {
    this.regioes = data;
  }

  onClickAccount(event) {
    if (this.lolAccount.validar()) {
      let loading = this.loadingCtrl.create({ content: 'Please Wait...' });
      loading.present();
      this.apiLol.addLeagueOfLegend(this.lolAccount).subscribe(
        (data: LeagueOfLegends) => {
          if (data.status == "Erro") {
            let alert = this.alertCtrl.create({
              title: 'Error',
              subTitle: data.mensagem,
              buttons: ['OK']
            });
            alert.present();
          }
          else {
            console.log("Registrado LoL: ");
            console.log(data);
            this.vincularLolComUsuario(data);
          }
          loading.dismiss();
        },
        erro => {
          loading.dismiss();          
          alert(erro);
        }
      );
    }
    else {
      alert("Check empty fields");
    }
  }
  
  vincularLolComUsuario(lol: LeagueOfLegends){
    this.usuario.leagueOfLegends=lol;
    this.usuario.leagueOfLegendsId = lol.leagueOfLegendsId;
    let loading = this.loadingCtrl.create({ content: 'Please Wait...' });
    loading.present();
    this.apiUsuario.putUsuario(this.usuario).subscribe(
      usuario => {
        console.log("Atualizado:");
        console.log(usuario);
        loading.dismiss();
        this.viewCtrl.dismiss(lol);
      },
      erro => {
        console.log("Erro ao atualizar:");
        console.log(erro);
        let alert = this.alertCtrl.create({
          title: 'Fail to register',
          buttons: ['OK']
        });
        alert.present();
        loading.dismiss();
      }
    );
  }

  onClickHeroiPrimario() {
    let modalHeroi = this.modalCtrl.create(SelecionarHeroiModalPage);
    modalHeroi.present();
    modalHeroi.onDidDismiss(
      (heroi: Heroi) => this.setarHeroi(heroi, 'primario')
    );
  }

  onClickHeroiSecundario() {
    let modalHeroi = this.modalCtrl.create(SelecionarHeroiModalPage);
    modalHeroi.present();
    modalHeroi.onDidDismiss(
      (heroi: Heroi) => this.setarHeroi(heroi, 'secundario')
    );
  }

  setarHeroi(heroi: Heroi, qual) {
    switch (qual) {
      case 'primario':
        this.lolAccount.heroiPrincipal = heroi;
        this.lolAccount.heroiPrincipalId = heroi.heroiId;
        break;
      case 'secundario':
        this.lolAccount.heroiSecundario = heroi;
        this.lolAccount.heroiSecundarioId = heroi.heroiId;
        break;
    }
  }

  onChange(radio) {
    if (radio.target.name == 'primary') {
      this.lolAccount.lanePrincipalId = radio.target.defaultValue;
    }
    if (radio.target.name == 'secundary') {
      this.lolAccount.laneSecundariaId = radio.target.defaultValue;
    }
  }

}
