import { UsuarioServiceProvider } from './../../providers/usuario-service/usuario-service';
import { Usuario } from './../../models/Usuario';
import { HomePage } from './../home/home';
import { LeagueOfLegends } from './../../models/LeagueOfLegends';
import { Jogo } from './../../models/Jogo';
import { Http } from '@angular/http';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, App, ViewController, AlertController, LoadingController } from 'ionic-angular';

import { LolAccountModalPage } from './../lol-account-modal/lol-account-modal';
import { JogoServiceProvider } from './../../providers/jogo-service/jogo-service';
/**
 * Generated class for the TelaIntegracaoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tela-integracao',
  templateUrl: 'tela-integracao.html',
  providers: [JogoServiceProvider]
})
export class TelaIntegracaoPage {

  usuario: Usuario;
  completar: boolean = false;
  games: Jogo[] = [
    { nome: "League of Legens", jogoId: 0, iconUrl: "http://orig13.deviantart.net/fece/f/2012/164/3/3/league_of_legends_dock_icon_by_kaldrax-d53bbj5.png" },
    // { nome: "Heroes of the Storm", jogoId: 1, iconUrl: "http://www.e-sportsinformer.com/wp-content/uploads/2016/09/hots-icon.jpg" }
  ];

  readyToNext = false;

  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private http: Http,
    private apiUsuario: UsuarioServiceProvider,
    private jogoService: JogoServiceProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public appCtrl: App) {
    this.usuario = navParams.data as Usuario;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TelaIntegracaoPage');
    //this.jogoService.getJogos();
  }

  onClickGameAccount(event, game: Jogo) {
    let modal = this.modalCtrl.create(LolAccountModalPage, { gameEscolhido: game, usuarioEscolhido: this.usuario });
    modal.present();
    modal.onDidDismiss((data: LeagueOfLegends) => {

      if (data == null) {
        return;
      }

      this.completar = true;

      // check response type
      game.adicionado = true;
      game.nomeConta = (<LeagueOfLegends>data).nick;
      game.iconUrl = (<LeagueOfLegends>data).imagem;

      // TODO Corrigir para quando tiver mais games
      this.readyToNext = true;
      this.usuario.leagueOfLegendsId = data.leagueOfLegendsId;
    });
  }

  onClickNext() {
    // this.viewCtrl.dismiss();
    // this.appCtrl.getRootNav().push(HomePage);
    this.navCtrl.popToRoot();

  }
}
