import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TelaIntegracaoPage } from './tela-integracao';

@NgModule({
  declarations: [
    TelaIntegracaoPage,
  ],
  imports: [
    IonicPageModule.forChild(TelaIntegracaoPage),
  ],
})
export class TelaIntegracaoPageModule {}
