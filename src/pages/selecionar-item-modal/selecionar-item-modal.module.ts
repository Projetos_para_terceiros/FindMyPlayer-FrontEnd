import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelecionarItemModalPage } from './selecionar-item-modal';

@NgModule({
  declarations: [
    SelecionarItemModalPage,
  ],
  imports: [
    IonicPageModule.forChild(SelecionarItemModalPage),
  ],
})
export class SelecionarItemModalPageModule {}
