import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MatchMainPage } from './match-main';

@NgModule({
  declarations: [
    MatchMainPage,
  ],
  imports: [
    IonicPageModule.forChild(MatchMainPage),
  ],
})
export class MatchMainPageModule {}
