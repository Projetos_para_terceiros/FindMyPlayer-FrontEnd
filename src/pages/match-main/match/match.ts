import { Relacionamento } from './../../../models/Relacionamento';
import { RelacionamentoServiceProvider } from './../../../providers/relacionamento-service/relacionamento-service';
import { Observable } from 'rxjs/Observable';
import { HeroisServiceProvider } from './../../../providers/herois-service/herois-service';
import { LeagueOfLegends_Regiao } from './../../../models/LeagueOfLegends_Regiao';
import { LeagueoflegendsServiceProvider } from './../../../providers/leagueoflegends-service/leagueoflegends-service';
import { LeagueOfLegends } from './../../../models/LeagueOfLegends';
import { Component } from '@angular/core';
import { IonicPage, ItemSliding, NavController, NavParams } from 'ionic-angular';

import { Usuario } from "../../../models/Usuario";
import { UsuarioServiceProvider } from "../../../providers/usuario-service/usuario-service";

/**
 * Generated class for the MatchPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-match',
  templateUrl: 'match.html',
})
export class MatchPage {

  usuarios: Usuario[];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private apiUsuario: UsuarioServiceProvider,
    private apiLol: LeagueoflegendsServiceProvider,
    private apiHeroi: HeroisServiceProvider,
    private apiRelacionamento: RelacionamentoServiceProvider) {
  }

  ionViewDidLoad() {
    this.recuperarLista();
  }

  recuperarLista() {
    this.apiUsuario.getUsuarios().subscribe(
      resp => {
        this.usuarios = resp.filter(a => a.leagueOfLegendsId != null);
      },
      err => alert(err),
      () => {
        this.recuperarLol();
      }
    );
  }

  recuperarLol() {
    this.usuarios.forEach((element, index) => {
      this.receberLol(index, element.leagueOfLegendsId);

    });
  }

  recuperarHeroi(current: number) {
    Observable.forkJoin(
      this.apiHeroi.getHeroi(this.usuarios[current].leagueOfLegends.heroiPrincipalId),
      this.apiHeroi.getHeroi(this.usuarios[current].leagueOfLegends.heroiSecundarioId)
    )
      .subscribe(
      data => {
        this.usuarios[current].leagueOfLegends.heroiPrincipal = data[0];
        this.usuarios[current].leagueOfLegends.heroiSecundario = data[1];
      }
      );
  }

  receberLol(current: number, lol: number) {
    this.apiLol.getLeagueOfLegend(lol).subscribe(
      resp => {
        this.usuarios[current].leagueOfLegends = new LeagueOfLegends(resp);
      },
      err => {
        console.log(err);
      },
      () => { this.recuperarHeroi(current); }
    );
  }

  onMatch(item, usuario: Usuario) {
    console.log(item);

    let rel: Relacionamento = {
      usuario1Id: this.apiUsuario.getUsuarioLogado().usuarioId,
      usuario2Id: usuario.usuarioId,
      tipoRelacionamentoId: 1
    }
    this.apiRelacionamento.addRelacionamento(rel).subscribe(
      () => {
        let index = this.usuarios.indexOf(usuario);
        if (index !== -1) {
          this.usuarios.splice(index, 1);
        }
      }
    );


  }

  onIgnorar(item, usuario: Usuario) {
    console.log(item);

    let rel: Relacionamento = {
      usuario1Id: this.apiUsuario.getUsuarioLogado().usuarioId,
      usuario2Id: usuario.usuarioId,
      tipoRelacionamentoId: 2
    }
    this.apiRelacionamento.addRelacionamento(rel).subscribe(
      () => {
        let index = this.usuarios.indexOf(usuario);
        if (index !== -1) {
          this.usuarios.splice(index, 1);
        }
      }
    );

  }

  procurarImagemLane(id) {
    switch (id) {
      case 1:
        return `assets/icon/lanes-icon/top_icon.png`
      case 2:
        return `assets/icon/lanes-icon/mid_icon.png`
      case 3:
        return `assets/icon/lanes-icon/bot_icon.png`
      case 4:
        return `assets/icon/lanes-icon/jungle_icon.png`
    }
  }

}
