import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { MatchPage } from './match/match';
import { MeusMatchesPage } from "./meus-matches/meus-matches";
/**
 * Generated class for the MatchMainPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-match-main',
  templateUrl: 'match-main.html',
})
export class MatchMainPage {

  tabMatch: any;
  tabMeusMatches: any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tabMatch = MatchPage;
    this.tabMeusMatches = MeusMatchesPage;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MatchMainPage');
  }

}
