import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChampionCountersPage } from './champion-counters';

@NgModule({
  declarations: [
    ChampionCountersPage,
  ],
  imports: [
    IonicPageModule.forChild(ChampionCountersPage),
  ],
})
export class ChampionCountersPageModule {}
