import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChampionUserbuildsPage } from './champion-userbuilds';

@NgModule({
  declarations: [
    ChampionUserbuildsPage,
  ],
  imports: [
    IonicPageModule.forChild(ChampionUserbuildsPage),
  ],
})
export class ChampionUserbuildsPageModule {}
