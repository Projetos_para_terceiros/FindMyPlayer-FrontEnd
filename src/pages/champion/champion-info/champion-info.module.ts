import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChampionInfoPage } from './champion-info';

@NgModule({
  declarations: [
    ChampionInfoPage
  ],
  imports: [
    IonicPageModule.forChild(ChampionInfoPage),
  ],
})
export class ChampionInfoPageModule {}
