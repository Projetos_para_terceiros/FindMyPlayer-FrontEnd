import { Usuario } from './../../models/Usuario';
import { UsuarioServiceProvider } from './../../providers/usuario-service/usuario-service';
import { Component, NgModule } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn } from "@angular/forms"

import { TelaIntegracaoPage } from './../tela-integracao/tela-integracao';
/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
  providers: [UsuarioServiceProvider]
})
export class RegisterPage {

  passwordMismatch = false;
  emailMismatch = false;

  registro: Usuario;

  registerForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private formBuider: FormBuilder,
              private apiUsuario: UsuarioServiceProvider,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,) {
    this.registro = new Usuario();
    this.registerForm = this.formBuider.group({
      email: ['', [Validators.email, Validators.required]],
      emailConfirmation: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.minLength(6), Validators.required]],
      passwordConfirmation: ['', [Validators.minLength(6), , Validators.required]]
    });
  }

  validateEqual(referencia: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      // self value
      let v = control.value;

      // value not equal
      if (referencia && v !== referencia) {
        return {
          validateEqual: false
        }
      }
      return {
        validateEqual: true
      }
    };
  }

  ionViewDidLoad() {
  }

  register(usuario: Usuario) {
    //this.navCtrl.push(TelaIntegracaoPage, this.registro);
    let loading = this.loadingCtrl.create({ content: 'Please Wait...' });
    loading.present();
    this.apiUsuario.addUsuario(usuario).subscribe(
      usuario => {
        console.log("Registrado:");
        console.log(usuario);
        this.navCtrl.push(TelaIntegracaoPage, usuario);
        loading.dismiss();
      },
      erro => {
        console.log("Erro ao registrar:");
        console.log(erro);
        let alert = this.alertCtrl.create({
          title: 'Fail to register',
          buttons: ['OK']
        });
        alert.present();
        loading.dismiss();
      }
    );
  }

  registerFormSubmit() {
    this.registerForm.updateValueAndValidity();
    if (this.registerForm.valid && this.verificarPasswordIguais() && this.verificarEmailsIguals()) {
      let usuarioSalvar: Usuario = {
        email: this.registerForm.controls.email.value,
        senhaHash: this.registerForm.controls.password.value,
        tipoAtivacaoId: 2,
      };
      this.register(usuarioSalvar);
    }
    else {
      let alert = this.alertCtrl.create({
        title: 'Check invalid fields',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  validarCamposNaoValidados(input) {
    switch (input.ngControl.name) {
      case "passwordConfirmation":
        this.verificarPasswordIguais();
        break;
      case "emailConfirmation":
        this.verificarEmailsIguals();
        break;
    }
  }

  verificarEmailsIguals(){
    if (this.registro.emailConfirmation != this.registro.email) {
      this.registerForm.controls.emailConfirmation.markAsDirty();
      this.registerForm.controls.emailConfirmation.setErrors({ "mismatch": true });
      this.emailMismatch = true;
      return false;
    }
    return true;
  }

  verificarPasswordIguais(){

    if (this.registro.passwordConfirmation != this.registro.password) {
      this.registerForm.controls.passwordConfirmation.markAsDirty();
      this.registerForm.controls.passwordConfirmation.setErrors({ "mismatch": true });
      this.passwordMismatch = true;
      return false;
    }
    return true;
  }

  onFocusOut(evento) {
    this.validarCamposNaoValidados(evento);
  }

}
