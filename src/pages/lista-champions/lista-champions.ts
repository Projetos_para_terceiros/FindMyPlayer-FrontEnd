import { ChampionPage } from './../champion/champion';
import { HeroisServiceProvider } from './../../providers/herois-service/herois-service';
import { Heroi } from './../../models/Heroi';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ListaChampionsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lista-champions',
  templateUrl: 'lista-champions.html',
})
export class ListaChampionsPage {
  herois: Heroi[];
  
  constructor(private viewCtrl: ViewController,
    public navCtrl: NavController,
     public navParams: NavParams,
    private apiHeroi: HeroisServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListaChampionsPage');
    this.apiHeroi.getHerois().subscribe(
      (data: Heroi[]) => this.herois = data,
      erro => console.log(erro));
  }

  onClickSelectHeroi(evento, heroi) {
    this.navCtrl.push(ChampionPage,heroi);
  }
}
