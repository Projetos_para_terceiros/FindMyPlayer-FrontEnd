import { LeagueOfLegends_Regiao } from './../../models/LeagueOfLegends_Regiao';
import { GlobalVariable } from './../../global';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';

/*
  Generated class for the LeagueOfLegendsRegioesServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class LeagueoflegendsRegioesServiceProvider {

  constructor(public http: Http) {
    console.log('Hello LeagueOfLegends_RegiaoServiceProvider Provider');
  }

  getLeagueOfLegends_Regioes(): Observable<LeagueOfLegends_Regiao[]> {
    return this.http.get(GlobalVariable.BASE_API + "LeagueOfLegends_Regioes")
      .map(response => response.json() as LeagueOfLegends_Regiao[])
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')
      );
  }

  getLeagueOfLegends_Regiao(id: number): Observable<LeagueOfLegends_Regiao> {
    return this.http.get(GlobalVariable.BASE_API + "LeagueOfLegends_Regioes/" + id)
      .map(response => response.json() as LeagueOfLegends_Regiao)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')
      );
  }

}
