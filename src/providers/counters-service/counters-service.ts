import { Counter } from './../../models/Counter';
import { GlobalVariable } from './../../global';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the CountersServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class CountersServiceProvider {

  constructor(public http: Http) {
    console.log('Hello CountersServiceProvider Provider');
  }

  getCounter(heroiId: number):Observable<Counter[]>{
    return this.http.get(GlobalVariable.BASE_API + "counters/" +heroiId)
    .map(data => data.json() as Counter[])
    .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getContraCounter(heroiId: number):Observable<Counter[]>{
    return this.http.get(GlobalVariable.BASE_API + "counters/" +heroiId + "/contracounters")
    .map(data => data.json() as Counter[])
    .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

}
