import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { GlobalVariable } from './../../global';
import { Item } from './../../models/Item';

/*
  Generated class for the ItemServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ItemServiceProvider {

  constructor(public http: Http) {
    console.log('Hello ItemServiceProvider Provider');
  }

  getItems(): Observable<Item[]> {
    return this.http.get(GlobalVariable.BASE_API + "Itens")
      .map(data => data.json() as Item[])
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getItem(id): Observable<Item> {
    return this.http.get(GlobalVariable.BASE_API + "Itens/" + id)
      .map(response => response.json() as Item)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

}
