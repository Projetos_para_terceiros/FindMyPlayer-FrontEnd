import { LeagueoflegendsServiceProvider } from './../leagueoflegends-service/leagueoflegends-service';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Storage } from '@ionic/storage';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { GlobalVariable } from './../../global';
import { Usuario } from './../../models/Usuario';

/*
  Generated class for the UsuarioServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class UsuarioServiceProvider {

  usuarioLogado: Usuario;

  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(public http: Http,
    private storage: Storage,
    private apiLol: LeagueoflegendsServiceProvider) {
    console.log('Hello UsuarioServiceProvider Provider');
  }

  loginAutomatico(): Promise<boolean> {
    // Or to get a key/value pair
    return this.storage.get('usuarioLogado').then((val) => {
      if (val) {
        console.log('Usuário Logado: ', val);
        this.usuarioLogado = val as Usuario;
        return true;
      }
      else {
        console.log('Nenhum usuario logado: ', val);
        return false;
      }
    })
      .catch(
      err => {
        console.log("Erro ao logar" + err);
        return false;
      });
  }

  login(usuario: string, senha: string): Observable<Usuario> {
    this.storage.remove('usuarioLogado');

    //TODO REMOVER EM Produção
    if (usuario == 'teste' && senha == 'teste') {
      return new Observable(
        (observer) => {
          let usuario: Usuario = {
            usuarioId: 27,
            email: 'viniciuslln@gmail.com',
            tipoAtivacaoId: 2,
            senhaHash: 'teste',
            status: 'OK'
          };
          this.usuarioLogado = usuario;
          this.storage.set('usuarioLogado', usuario);
          observer.next(usuario);

        }
      )
    }
    else
      return this.http.get(GlobalVariable.BASE_API + "usuarios/login")
        .map(response => {
          let usuario = response.json() as Usuario;
          if (usuario && usuario.status == 'OK') {
            this.usuarioLogado = usuario;
            this.storage.set('usuarioLogado', usuario);
          }
          return usuario;
        })
        .catch((error: any) => {
          this.storage.remove('usuarioLogado');
          return Observable.throw(error.json().error || 'Server error');
        });
  }

  logout(): Promise<boolean> {
    return new Promise(() => {
      this.storage.remove('usuarioLogado');
      return true;
    });
  }

  getUsuario(): Observable<Usuario> {
    return this.http.get(GlobalVariable.BASE_API + "usuarios")
      .map(response => {
        return response.json() as Usuario;
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getUsuarios(): Observable<Usuario[]> {
    return this.http.get(GlobalVariable.BASE_API + "usuarios")
      .map(response => {
        return response.json() as Usuario[];
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  addUsuario(usuario: Usuario): Observable<Usuario> {
    let usu = JSON.stringify(usuario);
    console.log(usu);
    let header = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(GlobalVariable.BASE_API + "usuarios", usu, { headers: header })
      .map(response => {
        return response.json() as Usuario;
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  putUsuario(usuario: Usuario): Observable<Usuario> {
    let usu = JSON.stringify(usuario,
      [
        'usuarioId',
        'email',
        'senhaHash',
        'leagueOfLegendsId',
        'tipoAtivacaoId',
      ]
    );
    console.log(usu);

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.put(GlobalVariable.BASE_API + "usuarios/" + usuario.usuarioId, usu, { headers: headers })
      .map(response => {
        return response.json() as Usuario;
      })
      .catch((error: any) => {
        return Observable.throw(error || 'Server error')
      });
  }

  getUsuarioLogado(): Usuario {
    return this.usuarioLogado as Usuario;
  }
}
