import { Observable } from 'rxjs/Rx';
import { Jogo } from './../../models/Jogo';
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

/*
  Generated class for the JogoServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class JogoServiceProvider {
  apiJogo = 'api/jogos';

  constructor(public http: Http) {
    console.log('Hello JogoServiceProvider Provider');
  }

  getJogos(): Observable<Jogo[]> {
    return this.http.get(this.apiJogo)
      .map(response => response.json().data as Jogo[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
