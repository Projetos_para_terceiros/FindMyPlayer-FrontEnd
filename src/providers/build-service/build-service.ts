import { GlobalVariable } from './../../global';
import { Observable } from 'rxjs/Rx';
import { Build } from './../../models/Build';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the BuildServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class BuildServiceProvider {

  constructor(public http: Http) {
    console.log('Hello BuildServiceProvider Provider');
  }

  getBuilds(): Observable<Build[]> {
    return this.http.get(GlobalVariable.BASE_API + "Builds")
      .map(data => data.json() as Build[])
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  addBuild(build: Build): Observable<Build> {
    let usu = JSON.stringify(build, [
      'nome',
      'descricao',
      'usuarioId',
      'heroiId',
    ]);
    let header = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(GlobalVariable.BASE_API + "Builds", usu, { headers: header })
      .map(response => {
        return response.json() as Build;
      })
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }
}
