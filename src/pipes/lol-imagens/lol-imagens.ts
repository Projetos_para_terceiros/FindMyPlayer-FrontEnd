import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the LolImagensPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'lolImagens',
})
export class LolImagensPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, tipo: string, skin = 0) {
    return this.getUrlPrefixo(value, tipo,  skin);
  }

  getUrlPrefixo(prefixo: string, tipo: string, skin: number) {
    switch (tipo) {
      case 'profile':
        return `http://ddragon.leagueoflegends.com/cdn/6.24.1/img/profileicon/${prefixo}`
      case 'champion_splash':
        return `http://ddragon.leagueoflegends.com/cdn/img/champion/splash/${prefixo}_${skin}.jpg`;
      case 'champion_square':
        return `http://ddragon.leagueoflegends.com/cdn/6.24.1/img/champion/${prefixo}.png`;
      case 'item': 
        return `http://ddragon.leagueoflegends.com/cdn/6.24.1/img/item/${prefixo}`;
      case ``:
        break;
    }
  }
}

// Champions (Loading Screen Art):
// http://ddragon.leagueoflegends.com/cdn/img/champion/loading/Aatrox_0.jpg

// Passive Abilities: 
// http://ddragon.leagueoflegends.com/cdn/6.24.1/img/passive/Cryophoenix_Rebirth.png 

// Champion Abilities: 
// http://ddragon.leagueoflegends.com/cdn/6.24.1/img/spell/FlashFrost.png 

// Summoner Spells: 
// http://ddragon.leagueoflegends.com/cdn/6.24.1/img/spell/SummonerFlash.png 

// Masteries: 
// http://ddragon.leagueoflegends.com/cdn/6.24.1/img/mastery/6111.png 

// Runes:
// http://ddragon.leagueoflegends.com/cdn/6.24.1/img/rune/8001.png 


// Sprites:
// http://ddragon.leagueoflegends.com/cdn/6.24.1/img/sprite/spell0.png 

// Minimaps (version 6.8.1):
// http://ddragon.leagueoflegends.com/cdn/6.8.1/img/map/map11.png 