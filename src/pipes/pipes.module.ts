import { NgModule } from '@angular/core';
import { LolImagensPipe } from './lol-imagens/lol-imagens';
import { LanesImagesPipe } from './lanes-images/lanes-images';
@NgModule({
	declarations: [LolImagensPipe,
    LanesImagesPipe],
	imports: [],
	exports: [LolImagensPipe,
    LanesImagesPipe]
})
export class PipesModule {}
